package middleware

import (
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
)

func IsLoggedIn(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.String() != "/login" && r.URL.String() != "/refresh" {
			log.Println(r.URL.String())

			headerToken, err := extractTokenFromHeader(r)
			if err != nil {
				w.WriteHeader(http.StatusUnauthorized)
				next.ServeHTTP(w, r)
				return
			}

			token, err := jwt.Parse(headerToken, func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("Unexpeceted signing method: %v", token.Header["alg"])
				}
				return []byte("fsociety"), nil
			})

			if err == nil && token.Valid {
				next.ServeHTTP(w, r)
				return
			}

			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte(`"error": "You are not authorized to access this content. Please login first."`))
			return

		}
		next.ServeHTTP(w, r)
	})
}

func IsAdmin(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.HasPrefix(r.URL.String(), "/admin/") {
			headerToken, err := extractTokenFromHeader(r)
			if err != nil {
				w.WriteHeader(http.StatusUnauthorized)
				next.ServeHTTP(w, r)
				return
			}
			token, err := jwt.Parse(headerToken, func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("Unexpeceted signing method: %v", token.Header["alg"])
				}
				return []byte("fsociety"), nil
			})

			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			claims := token.Claims.(jwt.MapClaims)
			isAdmin := claims["admin"].(bool)
			if !isAdmin {
				w.WriteHeader(http.StatusForbidden)
				return
			}
		}

		next.ServeHTTP(w, r)
	})
}

func extractTokenFromHeader(r *http.Request) (string, error) {
	authHeader := strings.Split(r.Header.Get("Authorization"), " ")
	if len(authHeader) < 2 {
		return "", fmt.Errorf("don't get a authorization token")
	}
	return authHeader[1], nil

}
