package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/thesilk/jwt-api/handlers"
	"gitlab.com/thesilk/jwt-api/middleware"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	h := handlers.Handler{}

	r := mux.NewRouter()
	r.HandleFunc("/login", h.Login).Methods("POST")
	r.HandleFunc("/refresh", h.Refresh)
	r.HandleFunc("/logout", h.NotImplemented)
	r.HandleFunc("/protected", h.Protected)
	r.HandleFunc("/admin/settings", h.Protected)
	r.Use(middleware.IsLoggedIn)
	r.Use(middleware.IsAdmin)

	log.Println("starting api server on localhost:1234...")
	log.Fatal(http.ListenAndServe(":1234", r))

}
