package handlers

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/thesilk/jwt-api/models"
)

type Handler struct{}

func (h *Handler) NotImplemented(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Not implemented"))
}

func (h *Handler) Login(w http.ResponseWriter, r *http.Request) {
	var (
		credentials models.Credentials
		tokenPair   models.TokenPair
		err         error
	)

	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	defer r.Body.Close()

	if err := json.Unmarshal(body, &credentials); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	if (credentials.Username == "admin" && credentials.Password == "secret") || credentials.Username == "guest" {
		tokenPair, err = generateTokenPair("admin")
		if err != nil {
			respondWithError(w, http.StatusInternalServerError, err.Error())
			return
		}

		respondWithJSON(w, http.StatusOK, tokenPair)
		return
	}

	respondWithStatusCode(w, http.StatusUnauthorized)
}

func (h *Handler) Protected(w http.ResponseWriter, r *http.Request) {
	respondWithJSON(w, http.StatusOK, map[string]string{
		"message": "This route is protected",
	})
}

func (h *Handler) Refresh(w http.ResponseWriter, r *http.Request) {
	var tokenPair models.TokenPair

	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		log.Println(err)
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	defer r.Body.Close()

	if err := json.Unmarshal(body, &tokenPair); err != nil {
		log.Println(err)
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	token, err := jwt.Parse(tokenPair.RefreshToken, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte("fsociety"), nil
	})

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		// check user id from db
		if int(claims["sub"].(float64)) == 1 {
			// get user from db
			tokenPair, err = generateTokenPair("admin")
			if err != nil {
				log.Println(err)
				respondWithError(w, http.StatusInternalServerError, err.Error())
				return
			}

			respondWithJSON(w, http.StatusOK, tokenPair)
			return
		}
	}
	respondWithStatusCode(w, http.StatusUnauthorized)
}
