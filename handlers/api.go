package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/thesilk/jwt-api/models"
)

func respondWithError(w http.ResponseWriter, status int, message string) {
	respondWithJSON(w, status, map[string]string{"error": message})
}

func respondWithJSON(w http.ResponseWriter, status int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write(response)
}

func respondWithStatusCode(w http.ResponseWriter, status int) {
	w.WriteHeader(status)
}

func generateTokenPair(username string) (models.TokenPair, error) {
	var (
		tokenPair models.TokenPair
		err       error
	)

	tokenPair.AccessToken, err = generateAccessToken("admin")
	if err != nil {
		return models.TokenPair{}, fmt.Errorf("Couldn't generate access token")
	}

	tokenPair.RefreshToken, err = generateRefreshToken()
	if err != nil {
		return models.TokenPair{}, fmt.Errorf("Couldn't generate access token")
	}

	return tokenPair, nil

}

func generateAccessToken(username string) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)
	claims["name"] = username
	// user id
	claims["sub"] = 1

	if username == "admin" {
		claims["admin"] = true
	} else {
		claims["admin"] = false
	}
	claims["exp"] = time.Now().Add(time.Minute).Unix()

	t, err := token.SignedString([]byte("fsociety"))
	if err != nil {
		return "", err
	}

	return t, nil
}

func generateRefreshToken() (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)
	// user id
	claims["sub"] = 1
	claims["exp"] = time.Now().Add(time.Hour * 72).Unix()

	t, err := token.SignedString([]byte("fsociety"))
	if err != nil {
		return "", err
	}

	return t, nil
}
